import React, { useEffect, useRef } from 'react';
import classes from './Cockpit.css';
import AuthContext from '../../context/auth-context'

const cockpit = props => {

  const toggleBtnRef = useRef(null);

  useEffect(() => {
    console.log('Cockpit -- useEffect');
    //Http request...
    // setTimeout(() => {
    //   alert('Save data to cloud!')
    // }, 100000000);
    toggleBtnRef.current.click();
    return () => {
      console.log('Cockpit cleanup work in useEffect');
    }
  }, []);

  useEffect(() => {
    console.log('Cockpit -- Second useEffect');
    return () => {
      console.log('Cockpit cleanup work in  Second useEffect');
    }
  })

  const assignedClasses = [];
  var btnClass = '';
  if (props.showPersons) {
    btnClass = classes.Red;
  }
  var caption = (<p className={assignedClasses.join(' ')}>This is really working!</p>)
  if (props.personsLength <= 2) {
    assignedClasses.push(classes.red);
    caption = <p className={assignedClasses.join(' ')}>This is really working!</p>
  }
  if (props.personsLength <= 1) {
    assignedClasses.push(classes.bold);
    caption = <p className={assignedClasses.join(' ')}>This is really working!</p>
  }
  if (props.personsLength === 0) {
    caption = null;
  }
  return (
    <div className={classes.Cockpit}>
      <h1>{props.title}</h1>
      {caption}
      <button ref={toggleBtnRef} className={btnClass} onClick={props.click}>Toggle Persons</button>
      <AuthContext.Consumer>
        {(context) =>
          <button onClick={context.login}>Log in</button>
        }
      </AuthContext.Consumer>
    </div>

  )
}

export default React.memo(cockpit);