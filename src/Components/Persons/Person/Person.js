import React, { Component } from 'react';
import classes from './Person.css';
import Auxiliary from '../../../HOC/Auxiliary';
import withClass from '../../../HOC/withClass';
import PropTypes from 'prop-types';
import AuthContext from '../../../context/auth-context'

class Person extends Component {
    constructor(props) {
        super(props);
        this.inputElementRef = React.createRef();
    }

    componentDidMount() {
        // this.inputElement.focus();
        this.inputElementRef.current.focus();
    }

    render() {
        console.log('Person.js ---rendering');
        return (
            <Auxiliary>
                <AuthContext.Consumer>
                    {
                        (context) => 
                        context.authenticated ? <p>Authenticated</p> : <p>Please log in!</p>
                    }
                </AuthContext.Consumer>
                            <p onClick={this.props.click}>Hi, I`m {this.props.name} and I`m {this.props.age} years old!</p>
                            <p key="i2">{this.props.children}</p>
                            <input
                                key="i3"
                                // ref={(inputEl) => {this.inputElement = inputEl}}
                                ref={this.inputElementRef}
                                type="text"
                                onChange={this.props.change}
                                value={this.props.name}>
                            </input>
            </Auxiliary>
        );
                }
            }
            
Person.propTypes = {
                    click: PropTypes.func,
                    change: PropTypes.func,
                    name: PropTypes.string,
                    age: PropTypes.number
                }
                
export default withClass(Person, classes.Person);