import React, { PureComponent } from 'react';
import Person from './Person/Person';

class Persons extends PureComponent {

  // static getDerivedStateFromProps(props, state){
  //   console.log('Persons -- getDerivedStateFromProps');
  // };

  // shouldComponentUpdate(nextProps, nextState){
  //   console.log('Persons -- shouldComponentUpdate');
  //   return nextProps.person !== this.props.persons || nextProps.clicked !== this.props.clicked ||
  //   nextProps.changed !== this.props.changed; 
  // };

  getSnapshotBeforeUpdate(prevProps, prevState){
    console.log('Persons -- getSnapshotBeforeUpdate');
    return {message: 'Snapshot!'};
  };

  componentDidUpdate(prevProps, prevState, snapshot){
    console.log('Persons -- componentDidUpdate');
    console.log(snapshot);
  };

  componentWillUnmount(){
    console.log('Perosns -- componentWillUnmount');
  }

  render() {
    console.log('Persons -- rendering...');
    return this.props.persons.map((person, index) => {
      return (
      <Person
        name={person.name}
        age={person.age}
        key={person.id}
        click={() => this.props.clicked(index)}
        change={(event) => this.props.changed(event, person.id)}
      />
      );
    });
  }
}

export default Persons;